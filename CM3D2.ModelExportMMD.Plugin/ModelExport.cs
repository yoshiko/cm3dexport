using CM3D2.ModelExportMMD.Gui;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityInjector;
using UnityInjector.Attributes;

namespace CM3D2.ModelExportMMD.Plugin
{
    [PluginName("CM3D2 ModelExport MMD")]
    [PluginVersion("1.3")]
    [PluginFilter("CM3D2x64")]
    [PluginFilter("CM3D2VRx64")]
    [PluginFilter("CM3D2x86")]
    public class ModelExport : PluginBase
    {
        private ExportWindow window;

        private List<SkinnedMeshRenderer> skinnedMeshList;

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.F7))
            {
                this.LoadModelMesh();
                this.window.Show();
            }
            if (Input.GetKeyDown(KeyCode.F8))
            {
                DumpComponentHierarchyOfModels();
            }
            if (Input.GetKeyDown(KeyCode.F9))
            {
                DumpGameObjects();
            }
        }

        private void PrintComponentsOf(Component root, string indent, HashSet<Component> printed)
        {
            string subindent = indent + indent[0];
            Debug.Log(indent + root.name + "(" + root.GetType().Name + ")");
            if (printed.Add(root))
            {
                List<Component> components = new List<Component>();
                root.GetComponents(components);
                foreach (Component c in components)
                {
                    PrintComponentsOf(c, subindent, printed);
                }
            }
            else
            {
                Debug.Log(subindent + "[already printed]");
            }
        }

        private void DumpComponentHierarchyOfModels()
        {
            SkinnedMeshRenderer[] array = UnityEngine.Object.FindObjectsOfType(typeof(SkinnedMeshRenderer)) as SkinnedMeshRenderer[];
            GameObject go = null;
            for (int i = 0; i < array.Length; i++)
            {
                go = array[i].gameObject;
                if (array[i].name.Contains("body"))
                {
                    break;
                }
            }
            Debug.Log("Component tree of " + go.name);
            List<Component> components = new List<Component>();
            go.GetComponents(components);
            string indent = ".";
            HashSet<Component> printed = new HashSet<Component>();
            foreach (Component c in components)
            {
                PrintComponentsOf(c, indent, printed);
            }
        }

        private void PrintComponentsToOf(StreamWriter w, Component root, string indent, HashSet<Component> printed)
        {
            string subindent = indent + indent[0];
            w.WriteLine(indent + root.name + "(" + root.GetType().Name + ")");
            if (printed.Add(root))
            {
                List<Component> components = new List<Component>();
                root.GetComponents(components);
                foreach (Component c in components)
                {
                    PrintComponentsToOf(w, c, subindent, printed);
                }
            }
            else
            {
                w.WriteLine(subindent + "[already printed]");
            }
        }

        private void DumpGameObjects()
        {
            GameObject[] _gos = UnityEngine.Object.FindObjectsOfType<GameObject>();
            List<GameObject> gos = new List<GameObject>(new HashSet<GameObject>(_gos));
            gos.Sort(delegate (GameObject go1, GameObject go2)
            {
                return go1.name.CompareTo(go2.name);
            });
            StreamWriter w = new StreamWriter("E:\\Downloads\\Custom Maid 3D 2 (ver 1.51.1+All DLC)\\CM3D2\\gos.txt");
            HashSet<Component> printed = new HashSet<Component>();
            foreach (GameObject go in gos)
            {
                w.WriteLine(go.name + "[" + go.GetInstanceID() + "]");
                foreach (Component c in go.GetComponents<Component>())
                {
                    PrintComponentsToOf(w, c, ".", printed);
                }
            }
            w.Close();
        }

        private void LoadModelMesh()
        {
            this.skinnedMeshList = new List<SkinnedMeshRenderer>();
            SkinnedMeshRenderer[] array = UnityEngine.Object.FindObjectsOfType(typeof(SkinnedMeshRenderer)) as SkinnedMeshRenderer[];
            foreach (SkinnedMeshRenderer skinnedMeshRenderer in array)
            {
                if (skinnedMeshRenderer.name != "obj1")
                {
                    Debug.Log("Found mesh " + skinnedMeshRenderer.name);
                    this.skinnedMeshList.Add(skinnedMeshRenderer);
                }
            }
        }

        private void ExportPMX()
        {
            string filename = ExportWindow.ExportFolder + "\\" + ExportWindow.ExportName + ".pmx";
            PmxBuilder pmxBuilder = new PmxBuilder();
            pmxBuilder.CreateModelInfo();
            pmxBuilder.PrepareData(skinnedMeshList);
            pmxBuilder.CreateBoneList();
            foreach (SkinnedMeshRenderer skinnedMesh in skinnedMeshList)
            {
                pmxBuilder.CreateMeshList(skinnedMesh);
            }
            pmxBuilder.CreatePmxHeader();
            pmxBuilder.Save(filename);
        }

        private void ExportOBJ()
        {
            ObjBuilder objBuilder = new ObjBuilder();
            string filename = ExportWindow.ExportFolder + "\\" + ExportWindow.ExportName + ".obj";
            objBuilder.Export(this.skinnedMeshList, filename);
        }

        private void ExportDAE()
        {
            string filename = ExportWindow.ExportFolder + "\\" + ExportWindow.ExportName + ".dae";
            DaeBuilder daeBuilder = new DaeBuilder(filename);
            foreach (SkinnedMeshRenderer skinnedMesh in skinnedMeshList)
            {
                daeBuilder.AddMesh(skinnedMesh);
            }
            daeBuilder.Finish();
        }

        private void OnGUI()
        {
            if (this.window == null)
            {
                this.window = new ExportWindow();
                this.window.PMXExportCallBack = (Action)Delegate.Combine(this.window.PMXExportCallBack, new Action(this.ExportPMX));
                this.window.OBJExportCallBack = (Action)Delegate.Combine(this.window.OBJExportCallBack, new Action(this.ExportOBJ));
                this.window.DAEExportCallBack = (Action)Delegate.Combine(this.window.DAEExportCallBack, new Action(this.ExportDAE));
            }
            this.window.DrawWindow();
        }
    }
}